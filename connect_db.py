# -*- coding: utf-8 -*-

import sqlite3
from sqlite3 import Error
 
class ConnectionDB(object):
    """docstring for ConnectionDB"""
    def __init__(self, arg):
        super(ConnectionDB, self).__init__()
        self.arg = arg
        
 
    def create_connection(db_file):
        """ create a database connection to the SQLite database
            specified by the db_file
        :param db_file: database file
        :return: Connection object or None
        """
        try:
            conn = sqlite3.connect(db_file)
            return conn
        except Error as e:
            print(e)
     
        return None