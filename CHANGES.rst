Changelog
=========

1.0a3 (14-07-2018)
------------------
Inclusão de testes com Selenium

1.0a2 (14-07-2018)
------------------
Salvando dados em uma base Sqlite

- Initial release.

1.0a1 (13-07-2018)
------------------
- Initial release.
- Extraindo dados com BeatifulSoup

