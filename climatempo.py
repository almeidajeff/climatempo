# -*- coding: utf-8 -*-

##########################################################################
# Object climatempo - Coleta de dados do site Climatempo
# Filename: climatempo.py
# Revision: 1.0
# Date: 13/07/2018
# Author: Jefferson Almeida - jralmeida88@gmail.com

#######################################################################

#######################################################################
# Imports
#######################################################################

# https://www.crummy.com/software/BeautifulSoup/bs3/documentation.html
try:
    from bs4 import BeautifulSoup  # To get everything
# version old
except ImportError:
    from BeautifulSoup import BeautifulSoup

from cidades_urls import UrlsClimaTempo
from connect_db import ConnectionDB
from urllib.request import urlopen
from utils import (normalize_title_city,
                   normalize_text,
                   datenow)


class Climatempo(object):
    """ docstring for Climatempo"""

    def __init__(self):
        super(Climatempo, self).__init__()

    def extractdata(site_url):
        html = urlopen(site_url).read()
        soup = BeautifulSoup(html, "lxml")

        ##############################################################
        # Extrair dados do site da previsão do dia
        ##############################################################

        # Cria Lista do conteudo
        list_city = []

        # Pega tag que possui Nome da Cidade e Sigla do Estado
        box_city = soup.find("p", {"class": "name-city-prev-home" })

        # Cria dicionario
        D = {}
        D['city'] = normalize_title_city(box_city.text)[0]
        D['state'] = normalize_title_city(box_city.text)[1]
        D['data_grid'] = []
        
        # Pega tabela completa
        table_data = soup.find('table', {'class':'left top20 small-12 border-none'})

        # Faz a iteração a partir da segunda linha da tabela
        for row in table_data.findAll('tr')[1:]:
            D_values = {}
            #import pdb;pdb.set_trace()
            D_values['month'] = row.findAll('td')[0].text
            D_values['minimum'] = row.findAll('td')[1].text
            D_values['maximum'] = row.findAll('td')[2].text
            D_values['precipitation'] = row.findAll('td')[3].text
            D['data_grid'].append(D_values)
        list_city.append(D)
        text_print = 'Foram coletados {0} dados da Cidade de {1}\
                      do Estado de {2}'.format(len(D['data_grid']),
                                                   D['city'],
                                                   D['state'])
        print (text_print)

        #TODO: criar metodo para isso
        conn = ConnectionDB.create_connection('climatempo.db')
        
        # definindo um cursor
        cursor = conn.cursor()
        list_city.append(datenow())
        # inserindo dados na tabela
        tupla = (list_city[0]['city'],
                 list_city[0]['state'],
                 list_city[0]['data_grid'][0]['month'],
                 list_city[0]['data_grid'][0]['minimum'],
                 list_city[0]['data_grid'][0]['maximum'],
                 list_city[0]['data_grid'][0]['precipitation'],
                 list_city[1])
        cursor.executemany("""
        INSERT INTO climatempo (nome_cidade, uf, mes, minima, maxima, precipitacao, criado_em)
        VALUES (?,?,?,?,?,?,?)
            """,[tupla])
        conn.commit()
        print ('Dados salvos na base de dados ')
        print ('************************************ \n')

    def create_table():
        """ Cria tabela para armanezar os dados """
        # Criando conexao
        conn = ConnectionDB.create_connection('climatempo.db')
        
        # definindo um cursor
        cursor = conn.cursor()

        # criando a tabela (schema)
        try:
            cursor.execute("""
            CREATE TABLE climatempo (
                    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                    nome_cidade TEXT NOT NULL,
                    uf VARCHAR(2) NOT NULL,
                    mes     TEXT ,
                    minima TEXT ,
                    maxima TEXT,
                    precipitacao TEXT,
                    criado_em DATE NOT NULL
            );
            """)
            print('Tabela criada com sucesso !')
        except:
            print ('Tabela já existe')
        conn.close()

def main():
    Climatempo.create_table()
    for url in UrlsClimaTempo.get_all_urls():
        Climatempo.extractdata(url)

if __name__ == '__main__':
    main()

