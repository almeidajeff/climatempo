***********************
# Climatempo

Software que faça a extração de alguns dados do site Clima Tempo.

Introdução
==========

Este pacote contém o código fonte do software para extração de dados do Site Climatempo, onde através das urls informadas é possível guardar os dados extraídos em um banco relacional SQLite.

Pré-requisitos
==============

A instalação deste pacote requer um pré-requisitos de softwares devem ser satisfeitos.
Os pré-requisitos estão descritos basicamente abaixo:


Software
--------

* Sistema operacional homologado (Ubuntu) atualizado na versão suportada mais recente
* Dependências de compilação do Python instaladas e atualizadas
* Ambiente virtual de Python 3.6 atualizado
* Instalação do `geckodriver` disponível em https://github.com/mozilla/geckodriver/releases

Instalação
==========

Para instalar o software execute os comandos abaixo.

    $ virtualenv-3.6 py36
    $ source py36/bin/activate
    $ python setup.py install

Execução
==========

Para executar o software utilize o comando abaixo.

    $ python climatempo.py

