# -*- coding: utf-8 -*-

import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class ClimatempoTest(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox(executable_path=r'/opt/projetos/geckodriver')

    def test_title_city(self):
        driver = self.driver
        driver.get("https://www.climatempo.com.br/climatologia/6/riobranco-ac")
        self.assertIn("Climatologia - Rio Branco - AC", driver.title)
        elem = driver.find_element_by_class_name("name-city-prev-home")
        if not elem.get_attribute("innerHTML"):
            assert "content not found." not in driver.page_source

    def test_table_contents(self):
        driver = self.driver
        driver.get("https://www.climatempo.com.br/climatologia/6/riobranco-ac")
        self.assertIn("Climatologia - Rio Branco - AC", driver.title)
        elem = driver.find_element_by_class_name("border-none")
        if not elem.get_attribute("innerHTML"):
            assert "content not found." not in driver.page_source        

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()